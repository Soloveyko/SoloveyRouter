<?php

use PHPUnit\Framework\TestCase;
use SoloveyRouter\Route;
use SoloveyRouter\Router;

/**
 * | -----------------------------
 * | Created by expexes on 9/26/18 8:12 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | RouteTest.php
 * | ---
 */
final class RouteTest extends TestCase
{

	public function testValidateRouteWithoutParameters()
	{
		$router = new Router();
		$route = new Route($router, 'GET', '/route', []);

		$this->assertTrue($route->validate('/route'));

		$this->assertFalse($route->validate('/'));
		$this->assertFalse($route->validate('/routes'));
		$this->assertFalse($route->validate('/123'));
		$this->assertFalse($route->validate('/123'));
	}

	public function testValidateRouteWithNumberParameter()
	{
		$router = new Router();
		$route = new Route($router, 'GET', '/route/{n}', []);

		$this->assertTrue($route->validate('/route/12'));

		$this->assertFalse($route->validate('/route/nuclear'));

		$this->assertFalse($route->validate('/'));
		$this->assertFalse($route->validate('/routes'));
		$this->assertFalse($route->validate('/123'));
		$this->assertFalse($route->validate('/123'));
	}

	public function testValidateRouteWithTwoNumberParameters()
	{
		$router = new Router();
		$route = new Route($router, 'GET', '/route/{n}/{n}', []);

		$this->assertTrue($route->validate('/route/11/22'));

		$this->assertFalse($route->validate('/route/kek/22'));
		$this->assertFalse($route->validate('/route/kek/lol'));
		$this->assertFalse($route->validate('/route/12'));
		$this->assertFalse($route->validate('/route/nuclear'));

		$this->assertFalse($route->validate('/'));
		$this->assertFalse($route->validate('/routes'));
		$this->assertFalse($route->validate('/123'));
		$this->assertFalse($route->validate('/123'));
	}

	public function testValidateRouteWithNonNumberParameter()
	{
		$router = new Router();
		$route = new Route($router, 'GET', '/route/{s}', []);

		$this->assertTrue($route->validate('/route/solo'));

		$this->assertFalse($route->validate('/route/69'));

		$this->assertFalse($route->validate('/'));
		$this->assertFalse($route->validate('/routes'));
		$this->assertFalse($route->validate('/123'));
		$this->assertFalse($route->validate('/123'));
	}

	public function testValidateRouteWithTwoNonNumberParameters()
	{
		$router = new Router();
		$route = new Route($router, 'GET', '/route/{s}/{s}', []);

		$this->assertTrue($route->validate('/route/solo/vey'));

		$this->assertFalse($route->validate('/route/kek/22'));
		$this->assertFalse($route->validate('/route/12'));
		$this->assertFalse($route->validate('/route/12/69'));
		$this->assertFalse($route->validate('/route/nuclear'));

		$this->assertFalse($route->validate('/'));
		$this->assertFalse($route->validate('/routes'));
		$this->assertFalse($route->validate('/123'));
		$this->assertFalse($route->validate('/123'));
	}

	public function testValidateRouteWithParameter()
	{
		$router = new Router();
		$route = new Route($router, 'GET', '/route/{id}', []);

		$this->assertTrue($route->validate('/route/pixel'));
		$this->assertTrue($route->validate('/route/78'));

		$this->assertFalse($route->validate('/route/kek/22'));
		$this->assertFalse($route->validate('/route/12/69'));
		$this->assertFalse($route->validate('/route/77/next'));

		$this->assertFalse($route->validate('/'));
		$this->assertFalse($route->validate('/routes'));
		$this->assertFalse($route->validate('/123'));
		$this->assertFalse($route->validate('/123'));
	}
}