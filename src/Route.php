<?php
/**
 * | -----------------------------
 * | Created by exp on 7/18/18 3:30 PM.
 * | Site: solovey.teslex.tech/router
 * | ------------------------------
 * | Route.php
 * | ---
 */


namespace SoloveyRouter;


class Route
{
	private $method;

	private $name;

	private $clearPattern;

	private $pattern;

	private $explodedPattern;

	private $controller;

	private $middlewares = [];

	private $match = [];

	private $router;

	/**
	 * Route constructor.
	 * @param Router $router
	 * @param string $method
	 * @param $pattern
	 * @param $controller
	 */
	public function __construct($router, $method, $pattern, $controller)
	{
		$this->router = $router;
		$this->method = explode('|', $method);
		$this->clearPattern = $pattern;
		$this->controller = $controller;

		$this->name = $pattern;

		$this->fixPattern();
	}

	public function fixPattern()
	{
		$pattern = $this->clearPattern;
		$ptk = implode('|', array_keys($this->router->getPatterns()));

		$pattern = $this->tr($pattern);
		$pattern = preg_replace_callback("/{($ptk|\w+)}/u", function ($m) {
			return array_key_exists($m[1], $this->router->getPatterns()) ? $this->router->getPatterns()[$m[1]] : '(.+)';
		}, $pattern);

		$pattern = preg_replace('/{(\(.+\))}/u', '\1', $pattern);

		$this->pattern = $pattern;
		$this->explodedPattern = explode("/", $pattern);
	}

	private function tr($url)
	{
		return preg_replace('/([\/]+)/u', '/', trim(strtok($url, '?'), '/'));
	}

	public function name($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @param $uri
	 * @return bool
	 */
	public function validate($uri)
	{
		$uri = urldecode(preg_replace('/[\/]+/', '/', trim($uri, '/')));
		$s_uri = explode("/", $uri);

		$s_pattern = $this->explodedPattern;

		$is_good = true;

		if (sizeof($s_uri) === sizeof($s_pattern) || (array_pop($s_pattern) === '*')) {

			for ($i = 0; $i < sizeof($s_pattern); $i++) {
				if ($s_pattern[$i] == "*")
					break;

				if (preg_match("/^{$s_pattern[$i]}$/u", $s_uri[$i], $match)) {
					if (preg_match_all("/([!\(.+\)]|[!\[.+\]])/u", $s_pattern[$i]) && isset($match[0]))
						array_push($this->match, $match[0]);
				} else {
					$is_good = false;
				}
			}

		} else {
			$is_good = false;
		}

		return $is_good;
	}

	public function solve()
	{
		if (!empty($this->middlewares)) {
			foreach ($this->middlewares as $middleware) {
				if (is_callable($middleware[0]))
					call_user_func_array($middleware[0], $this->match);
				else
					call_user_func_array([new $middleware[0], is_null($middleware[1]) ? 'index' : $middleware[1]], $this->match);
			}
		}

		if ($this->match)
			if (is_callable($this->controller[0]))
				call_user_func_array($this->controller[0], $this->match);
			else
				call_user_func_array([new $this->controller[0], isset($this->controller[1]) ? $this->controller[1] : 'index'], $this->match);
		else
			if (is_callable($this->controller[0]))
				call_user_func($this->controller[0]);
			else
				call_user_func([new $this->controller[0], isset($this->controller[1]) ? $this->controller[1] : 'index']);
	}

	/**
	 * @return array
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * @param $method
	 * @return Route
	 */
	public function method($method)
	{
		$this->method = $method;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getClearPattern()
	{
		return $this->clearPattern;
	}

	/**
	 * @return mixed
	 */
	public function getPattern()
	{
		return $this->pattern;
	}

	/**
	 * @param string $pattern
	 * @return Route
	 */
	public function pattern($pattern)
	{
		$this->pattern = $pattern;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getExplodedPattern()
	{
		return $this->explodedPattern;
	}

	/**
	 * @return mixed
	 */
	public function getController()
	{
		return $this->controller;
	}

	/**
	 * @param mixed $controller
	 * @return Route
	 */
	public function controller($controller)
	{
		$this->controller = $controller;
		return $this;
	}

	/**
	 * @return null
	 */
	public function getMiddlewares()
	{
		return $this->middlewares;
	}

	/**
	 * @param $call
	 * @param string $method
	 * @return Route
	 */
	public function middleware($call, $method = null)
	{
		array_push($this->middlewares, [$call, $method]);
		return $this;
	}

	/**
	 * @param $call
	 * @param string $method
	 * @return $this
	 */
	public function leftMiddleware($call, $method = null)
	{
		array_unshift($this->middlewares, [$call, $method]);
		return $this;
	}

	/**
	 * @return array
	 */
	public function getMatch()
	{
		return $this->match;
	}

	/**
	 * @param array $match
	 */
	public function setMatch($match)
	{
		$this->match = $match;
	}

	/**
	 * @return Router
	 */
	public function getRouter()
	{
		return $this->router;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}
}