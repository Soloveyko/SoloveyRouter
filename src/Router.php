<?php
/**
 * | -----------------------------
 * | Created by expexes on 7/18/18 3:34 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | Router.php
 * | ---
 */

namespace SoloveyRouter;


use SoloveyRouter\Exception\RouterException;

class Router
{
	private $root = '';
	private $routes = [];
	private $patterns = [
		'w' => '(\w+)',
		'n' => '(\d+)',
		's' => '([a-zA-Z\p{Cyrillic}]+)',
		'sn' => '([a-zA-Z\p{Cyrillic}\d]+)'
	];

	private $activeRoute;

	/**
	 * @param string $pattern
	 * @param $controller
	 * @param $action
	 * @return mixed
	 */
	public function post($pattern, $controller, $action = null)
	{
		$pattern = $this->root . $pattern;
		$r = new Route($this, 'POST', $pattern, [$controller, $action]);
		return $this->routes[array_push($this->routes, $r) - 1];
	}

	/**
	 * @param string $pattern
	 * @param $controller
	 * @param null $action
	 * @return mixed
	 */
	public function put($pattern, $controller, $action = null)
	{
		$pattern = $this->root . $pattern;
		$r = new Route($this, 'PUT', $pattern, [$controller, $action]);
		return $this->routes[array_push($this->routes, $r) - 1];
	}

	/**
	 * @param string $pattern
	 * @param $controller
	 * @param null $action
	 * @return mixed
	 */
	public function patch($pattern, $controller, $action = null)
	{
		$pattern = $this->root . $pattern;
		$r = new Route($this, 'PUT', $pattern, [$controller, $action]);
		return $this->routes[array_push($this->routes, $r) - 1];
	}

	/**
	 * @param string $pattern
	 * @param $controller
	 * @param null $action
	 * @return mixed
	 */
	public function delete($pattern, $controller, $action = null)
	{
		$pattern = $this->root . $pattern;
		$r = new Route($this, 'DELETE', $pattern, [$controller, $action]);
		return $this->routes[array_push($this->routes, $r) - 1];
	}

	/**
	 * @param string $pattern
	 * @param $controller
	 * @param null $action
	 * @return mixed
	 */
	public function options($pattern, $controller, $action = null)
	{
		$pattern = $this->root . $pattern;
		$r = new Route($this, 'OPTIONS', $pattern, [$controller, $action]);
		return $this->routes[array_push($this->routes, $r) - 1];
	}

	/**
	 * @param string $pattern
	 * @param $controller
	 * @param null $action
	 * @return mixed
	 */
	public function any($pattern, $controller, $action = null)
	{
		$pattern = $this->root . $pattern;
		$r = new Route($this, 'ANY', $pattern, [$controller, $action]);
		return $this->routes[array_push($this->routes, $r) - 1];
	}

	/**
	 * @param $pattern
	 * @param \Closure $callback
	 * @return Group
	 */
	public function group($pattern, $callback)
	{
		$tpl = clone $this;
		$tpl->root($tpl->getRoot() . $pattern);
		$callback($tpl);
		$this->routes(array_merge($tpl->routes, $this->routes));
		return new Group($this, $tpl->routes);
	}

	/**
	 * @return string
	 */
	public function getRoot()
	{
		return $this->root;
	}

	/**
	 * @param string $root
	 */
	public function root($root)
	{
		$this->root = $root;
	}

	/**
	 * @param string $pattern
	 * @param $controller
	 * @param null $action
	 * @return Route
	 */
	public function get($pattern, $controller, $action = null)
	{
		$pattern = $this->root . $pattern;
		$r = new Route($this, 'GET', $pattern, [$controller, $action]);
		return $this->routes[array_push($this->routes, $r) - 1];
	}

	/**
	 * @param Route $route
	 * @return mixed
	 */
	public function route($route)
	{
		return $this->routes[array_push($this->routes, $route) - 1];
	}

	/**
	 * @param $name
	 * @param $pattern
	 */
	public function pattern($name, $pattern)
	{
		$this->patterns[$name] = $pattern;
	}

	/**
	 * @param $uri
	 * @param string $method
	 * @throws RouterException
	 */
	public function go($uri, $method = 'GET')
	{
		$uri = isset($uri) ? $uri : '';
		$uri = strtok($uri, '?');

		$routes = array_filter($this->routes, function (Route $v) use ($method) {
			return in_array($method, $v->getMethod()) || in_array('ANY', $v->getMethod());
		});

		$routes = array_filter($routes, function (Route $v) use ($uri) {
			return $v->validate($uri);
		});

		if ($routes) {
			/** @var Route $route */
			$route = array_shift($routes);

			$this->activeRoute = $route;
			$route->solve();
		} else {
			throw new RouterException('Not Found', 404);
		}
	}

	/**
	 * @return array
	 */
	public function getRoutes()
	{
		return $this->routes;
	}

	/**
	 * @param array $routes
	 */
	public function routes($routes)
	{
		$this->routes = $routes;
	}

	/**
	 * @return array
	 */
	public function getPatterns()
	{
		return $this->patterns;
	}

	/**
	 * @param array $patterns
	 */
	public function patterns($patterns)
	{
		$this->patterns = $patterns;
	}

	/**
	 * @return mixed
	 */
	public function getActiveRoute()
	{
		return $this->activeRoute;
	}

	/**
	 * @param mixed $activeRoute
	 */
	public function activeRoute($activeRoute)
	{
		$this->activeRoute = $activeRoute;
	}
}