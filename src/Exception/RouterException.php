<?php
/**
 * | -----------------------------
 * | Created by exp on 4/10/18/12:46 AM.
 * | Site: teslex.tech
 * | ------------------------------
 * | RouterException.php
 * | ---
 */

namespace SoloveyRouter\Exception;


use Exception;

class RouterException extends Exception
{

	public function __construct($message, $code = 500, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}

}