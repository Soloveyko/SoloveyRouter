<?php
/**
 * | -----------------------------
 * | Created by expexes on 9/26/18 8:07 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | Group.php
 * | ---
 */

namespace SoloveyRouter;


class Group
{
	public $router;

	public $routes;

	/**
	 * Group constructor.
	 * @param Router $router
	 * @param $routes
	 */
	public function __construct(&$router, $routes)
	{
		$this->router = $router;
		$this->routes = $routes;
	}

	/**
	 * @param $call
	 * @param null $method
	 * @return $this
	 */
	public function middleware($call, $method = null)
	{
		/** @var Route $route */
		foreach ($this->router->getRoutes() as $route) {
			if (in_array($route, $this->routes)) {
				$route->leftMiddleware($call, $method);
			}
		}

		return $this;
	}

	/**
	 * @param $name
	 * @return $this
	 */
	public function name($name)
	{
		/** @var Route $route */
		foreach ($this->router->getRoutes() as $route) {
			if (in_array($route, $this->routes)) {
				$route->name($name . $route->getName());
			}
		}

		return $this;
	}
}